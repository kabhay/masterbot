/*-----------------------------------------------------------------------------
A simple Language Understanding (LUIS) bot for the Microsoft Bot Framework. 
-----------------------------------------------------------------------------*/

var restify = require('restify');
var builder = require('botbuilder');
var botbuilder_azure = require("botbuilder-azure");
var Swagger = require('swagger-client');
var open = require('open');
var rp = require('request-promise');
// config items
var pollInterval = 1000;
var directLineSecret = 'I7qGhwKSqZw.cwA.Xdw.MMLTsEtAGs-BFJ3miDHBnTuQ75_kf7yc0wtK3iiGWQE';
var directLineClientName = 'DirectLineClient';
var directLineSpecUrl = 'https://docs.botframework.com/en-us/restapi/directline3/swagger.json';

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});
  
// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: '8d6079bf-a767-4500-8caa-1549ea2103a4',
    appPassword: '&GZuy^^:.3MsRn7p',
    openIdMetadata: process.env.BotOpenIdMetadata 
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

/*----------------------------------------------------------------------------------------
* Bot Storage: This is a great spot to register the private state storage for your bot. 
* We provide adapters for Azure Table, CosmosDb, SQL Azure, or you can implement your own!
* For samples and documentation, see: https://github.com/Microsoft/BotBuilder-Azure
* ---------------------------------------------------------------------------------------- */

var tableName = 'botdata';
var azureTableClient = new botbuilder_azure.AzureTableClient(tableName, process.env['AzureWebJobsStorage']);
var tableStorage = new botbuilder_azure.AzureBotStorage({ gzipData: false }, azureTableClient);

// Create your bot with a function to receive messages from the user
// This default message handler is invoked if the user's utterance doesn't
// match any intents handled by other dialogs.
var bot = new builder.UniversalBot(connector, function (session, args) {
    session.send('You reached the default message handler. You said \'%s\'.', session.message.text);
});

bot.set('storage', tableStorage);
const logUserConversation = (event) => {
console.log('message: ' + event.text + ', user: ' + event.address.user.name);
    };
    
// Middleware for logging
bot.use({
    receive: function (event, next) {
        logUserConversation(event);
        next();
    },
    send: function (event, next) {
        logUserConversation(event);
        next();
    }
});

// Make sure you add code to validate these fields
var luisAppId = '75cde45d-5463-4063-803f-b5536e656501';
var luisAPIKey = '2eba3497040048af8d2651c2882521fe';
var luisAPIHostName = process.env.LuisAPIHostName || 'westus.api.cognitive.microsoft.com';

const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v2.0/apps/' + luisAppId + '?subscription-key=' + luisAPIKey;

// Create a recognizer that gets intents from LUIS, and add it to the bot
var recognizer = new builder.LuisRecognizer(LuisModelUrl);
bot.recognizer(recognizer);

// Add a dialog for each intent that the LUIS app recognizes.
// See https://docs.microsoft.com/en-us/bot-framework/nodejs/bot-builder-nodejs-recognize-intent-luis 
bot.dialog('GreetingDialog',
    (session) => {
        session.send('Hello %s, How may I help you today.', session.message.user.name);
		session.send('Please ask your query else type HELP to know more');
        session.endDialog();
    }
).triggerAction({
    matches: 'Greeting'
})
bot.dialog('kpibotDialog',
    (session) => {
        session.send('Hello %s, You have reached KPIBot. How may I help you today.', session.message.user.name);
		session.send('Please ask your query else type HELP to know more');
        session.endDialog();
    }
).triggerAction({
    matches: 'kpibot'
})
bot.dialog('AicoachBotDialog',
    (session) => {
        session.send('Hello %s, You have reached AICoach BOT. How may I help you today.', session.message.user.name);
		session.send('Please ask your query else type HELP to know more');
        session.endDialog();
    }
).triggerAction({
    matches: 'aicoach'
})    
bot.dialog('HelpDialog',
    (session) => {
        session.send('I am programmed to provide learning course recommendations based on your queries. Few frequently asked queries are:');
        session.send('. I am searching for courses to develop my leadership skills');
        session.send('. I am looking for courses to develop my problem solving skills');
        session.send('Courses Covered: Leadership, Communication, Problem Solving, Coaching, Mentoring and many more.');
        session.send('I can also help you with enrolling for your specific course of interest');
        session.send('Thank you - Please resume your chat');		
        session.endDialog();
    }
).triggerAction({
    matches: 'Help'
})
        
bot.dialog('CancelDialog',
    (session) => {
        session.send('You reached the Cancel intent. You said \'%s\'.', session.message.text);
        session.endDialog();
        }
).triggerAction({
    matches: 'Cancel'
})

